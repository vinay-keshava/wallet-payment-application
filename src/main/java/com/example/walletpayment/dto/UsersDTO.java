package com.example.walletpayment.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsersDTO {

    private Long id;

    @NotNull
    @Size(max = 255)
    private String userName;

    @NotNull
    @Email
    @Size(max = 255)
    private String contactNumber;

    @Size(max = 255)
    private String address;

    private Long walletId;

}
