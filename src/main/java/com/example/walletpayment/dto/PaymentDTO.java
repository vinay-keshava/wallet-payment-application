package com.example.walletpayment.dto;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PaymentDTO {

    private Long walletId;
    private Long orderId;

}
