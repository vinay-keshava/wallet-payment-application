package com.example.walletpayment.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ProductsDTO {

    @NotNull
    @Size(max = 255)
    private String productName;

    @NotNull
    private Double price;

    @NotNull
    private Integer availableQuantity;

}
