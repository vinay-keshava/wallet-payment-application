package com.example.walletpayment.dto;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WalletDTO {
	

	private String walletName;

    @Column
    private LocalDateTime validityDate;

    @Column(nullable = false)
    private double walletPoints;

}
