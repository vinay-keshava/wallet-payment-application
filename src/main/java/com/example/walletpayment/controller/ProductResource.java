package com.example.walletpayment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.walletpayment.dto.ProductsDTO;
import com.example.walletpayment.dto.ResponseDto;
import com.example.walletpayment.service.impl.ProductServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.constraints.Size;

@RestController
@RequestMapping("/api")

public class ProductResource {

	@Autowired
	ProductServiceImpl productImpl;

	@Operation(summary = "new product")
	@PostMapping("/product")
	public ResponseEntity<ResponseDto> newProduct(ProductsDTO productDto) {
		return new ResponseEntity<>(productImpl.newProduct(productDto), HttpStatus.CREATED);
	}

	@Operation(summary = "search produtcs")
	@GetMapping("/products")
	public ResponseEntity<List<ProductsDTO>> searchProduct(@RequestParam @Size(min = 3) String productName,
			@RequestParam int offset, @RequestParam int pageSize) {
		Page<ProductsDTO> matchingProducts = productImpl.searchProducts(productName, offset, pageSize);
		return ResponseEntity.ok(matchingProducts.getContent());
	}

}
