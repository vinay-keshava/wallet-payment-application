package com.example.walletpayment.exception;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String message;
	public ProductNotFoundException(String message) {
		super();
		this.message = message;

}
}
