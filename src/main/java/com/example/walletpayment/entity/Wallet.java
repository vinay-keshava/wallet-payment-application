package com.example.walletpayment.entity;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Wallet {
	
	
//    @Column(nullable = false, updatable = false)
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long walletId;
	
	private String walletName;

    @Column
    private LocalDateTime validityDate;

    @Column(nullable = false)
    private double walletPoints;
}
