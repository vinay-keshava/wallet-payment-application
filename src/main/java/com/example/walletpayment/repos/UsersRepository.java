package com.example.walletpayment.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.walletpayment.entity.User;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {
}
