package com.example.walletpayment.repos;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.walletpayment.entity.Products;

@Repository
public interface ProductsRepository extends JpaRepository<Products, Long> {

	Products findByProductNameIgnoreCase(String productName);

	Page<Products> findByProductNameContainingIgnoreCase(String productName, Pageable pageable);
}
