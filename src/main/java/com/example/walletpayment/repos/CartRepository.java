package com.example.walletpayment.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.walletpayment.entity.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {
}
