package com.example.walletpayment.service.impl;

import org.springframework.data.domain.Page;

import com.example.walletpayment.dto.ProductsDTO;
import com.example.walletpayment.dto.ResponseDto;

public interface ProductService {

	public ResponseDto newProduct(ProductsDTO productDto);

	public Page<ProductsDTO> searchProducts(String productName, int offset, int pageSize);

}
