package com.example.walletpayment.service.impl;

import java.util.Collections;
import java.util.Objects;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.walletpayment.dto.ProductsDTO;
import com.example.walletpayment.dto.ResponseDto;
import com.example.walletpayment.entity.Products;
import com.example.walletpayment.exception.ProductNotFoundException;
import com.example.walletpayment.repos.ProductsRepository;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductsRepository productsRepository;

	@Override
	public ResponseDto newProduct(ProductsDTO productDto) {
		if (Objects.isNull(productsRepository.findByProductNameIgnoreCase(productDto.getProductName()))) {
			Products product = new Products();
			BeanUtils.copyProperties(productDto, product);
			productsRepository.save(product);
			return new ResponseDto(Collections.singletonList("Product inserted successfully"), 201);
		} else {
			return new ResponseDto(Collections.singletonList("Product already present"), 409);
		}
	}

	@Override
	public Page<ProductsDTO> searchProducts(String productName, int offset, int pageSize) {
		Pageable pageable = PageRequest.of(offset, pageSize);
		Page<Products> products2 = productsRepository.findByProductNameContainingIgnoreCase(productName, pageable);
		if (products2.isEmpty()) {
			throw new ProductNotFoundException("No Matching Products Found");
		}
		return products2.map(product -> {
			ProductsDTO dto = new ProductsDTO();
			BeanUtils.copyProperties(product, dto);
			return dto;
		});

	}

}
