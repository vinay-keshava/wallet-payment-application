package com.example.walletpayment.controller;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.walletpayment.dto.ProductsDTO;
import com.example.walletpayment.service.impl.ProductServiceImpl;

import jakarta.validation.ConstraintViolationException;

@ExtendWith(SpringExtension.class)
public class ProductResourceTest {
	

	@Mock
	private ProductServiceImpl prodService;

	@InjectMocks
	private ProductResource productResource;
	
	@Test
    public void searchMatchingProductsTest() {
        String productName = "iPhone";
        int offset = 0;
	    int pageSize = 10;
        List<ProductsDTO> expectedProducts = Arrays.asList(new ProductsDTO(), new ProductsDTO());
        Page<ProductsDTO> matchingProducts = new PageImpl<>(expectedProducts);

        Mockito.when(prodService.searchProducts(productName,offset,pageSize)).thenReturn(matchingProducts);

        ResponseEntity<List<ProductsDTO>> response = productResource.searchProduct(productName,offset,pageSize);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertEquals(expectedProducts, response.getBody());
    }
	
	@Test
	public void searchProducthrowsConstraintViolationException() {
	    String productName = "iP";
	    int offset = 0;
	    int pageSize = 10;
	    Mockito.when(prodService.searchProducts(productName,offset,pageSize)).thenThrow(ConstraintViolationException.class);

	    ConstraintViolationException ex = Assert.assertThrows(ConstraintViolationException.class, () -> {
	    	productResource.searchProduct(productName,offset,pageSize);
	    });
	    Assert.assertNotNull(ex);
	}
	

}
