package com.example.walletpayment.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.walletpayment.dto.ProductsDTO;
import com.example.walletpayment.dto.ResponseDto;
import com.example.walletpayment.entity.Products;
import com.example.walletpayment.exception.ProductNotFoundException;
import com.example.walletpayment.repos.ProductsRepository;
import com.example.walletpayment.service.impl.ProductServiceImpl;

@ExtendWith(SpringExtension.class)
public class ProductServiceImplTest {
	
	@Mock
	private ProductsRepository productRep;

	@InjectMocks
	private ProductServiceImpl prodService;
	
	
	@Test
    public void testNewProductSuccess() {
        ProductsDTO productDto = new ProductsDTO();
        productDto.setProductName("Test Product");
        productDto.setPrice(20.5);
        productDto.setAvailableQuantity(30);

        Products product = new Products();
        BeanUtils.copyProperties(productDto, product);

        when(productRep.findByProductNameIgnoreCase(productDto.getProductName())).thenReturn(null);
        when(productRep.save(product)).thenReturn(product);

        ResponseDto responseDto = prodService.newProduct(productDto);

        assertNotNull(responseDto);
        assertEquals(Collections.singletonList("Product inserted successfully"), responseDto.getMessages());
    }
	
	@Test
    public void testNewProductFailure() {
        ProductsDTO productDto = new ProductsDTO();
        productDto.setProductName("New Product");
        productDto.setPrice(80.5);
        productDto.setAvailableQuantity(70);

        Products product = new Products();
        BeanUtils.copyProperties(productDto, product);

        when(productRep.findByProductNameIgnoreCase(productDto.getProductName())).thenReturn(product);

        ResponseDto responseDto = prodService.newProduct(productDto);

        assertNotNull(responseDto);
        assertEquals(Collections.singletonList("Product already present"), responseDto.getMessages());
    }
	
	@Test
	public void testSearchInvalidProduct() {

		String productName = "NotExist";
		int offset = 0;
	    int pageSize = 10;
		when(productRep.findByProductNameContainingIgnoreCase(any(String.class), any(Pageable.class)))
		.thenReturn(new PageImpl<>(new ArrayList<Products>()));

		assertThrows(ProductNotFoundException.class, () -> {
			prodService.searchProducts(productName, offset, pageSize);
		});
	}
	
	@Test
	public void testSearchPageOfProducts() {
	    String productName = "iPhone";
	    int offset = 0;
	    int pageSize = 10;

	    List<Products> products = Arrays.asList(new Products(), new Products());
	    Page<Products> productsPage = new PageImpl<>(products);

	    Mockito.when(productRep.findByProductNameContainingIgnoreCase(productName, PageRequest.of(offset, pageSize))).thenReturn(productsPage);

	    Page<ProductsDTO> result = prodService.searchProducts(productName, offset, pageSize);

	    Assert.assertEquals(productsPage.getTotalElements(), result.getTotalElements());
	    Assert.assertEquals(productsPage.getContent().size(), result.getContent().size());
	}

}
